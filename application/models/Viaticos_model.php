<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Viaticos_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE viaticos');
		return $this->db->insert("viaticos",$data);
	}

	public function update($data,$id){
		$this->db->where("id_viatico",$id);
		return $this->db->update("viaticos",$data);
	}

	public function getViatico($id){
		$this->db->select("v.*");
		$this->db->from("viaticos v");
		$this->db->where("v.id_viatico",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getViaticos(){
		$this->db->select("v.*");
		$this->db->from("viaticos v");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id_viatico", $id);
		$this->db->db_debug = false;
		if($this->db->delete("viaticos")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar un cliente activo!");
		}
	}

}