<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehiculo_model extends CI_Model {


    public function save($data){
		
		$this->db->query('ALTER TABLE vehiculos');
		return $this->db->insert("vehiculos",$data);
	}


	public function update($data,$id){
		$this->db->where("id_vehiculo",$id);
		return $this->db->update("vehiculos",$data);
	}

	public function getVehiculo($id){
		$this->db->select("v.*");
		$this->db->from("vehiculos v");
		$this->db->where("v.id_vehiculo",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getVehiculos(){
		$this->db->select("v.*");
		$this->db->from("vehiculos v");
		
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id_vehiculo", $id);
		$this->db->db_debug = false;
		if($this->db->delete("vehiculos")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar un usuario activo!");
		}
	}

	public function getRols(){
		$this->db->select("r.id, r.name");
		$this->db->from("rol r");
		$results = $this->db->get();
		return $results->result();
	}

	public function getId(){
		$this->db->select("v.id_vehiculo");
		$this->db->from("vehiculos v");
		$this->db->order_by("v.id_vehiculo","desc");
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->row()){
			return $result->row()->id_vehiculo+1;
		}else{
			return 0;
		}
	}

}