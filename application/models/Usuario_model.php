<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE usuarios');
		return $this->db->insert("usuarios",$data);
	}

	public function update($data,$id){
		$this->db->where("id",$id);
		return $this->db->update("usuarios",$data);
	}

	public function getUsuario($id){
		$this->db->select("u.*");
		$this->db->from("usuarios u");
		$this->db->where("u.id",$id);
		$result = $this->db->get();
		return $result->row(); 
	}
	
	public function getUsuarios(){
		$this->db->select("u.*");
		$this->db->from("usuarios u");
		
		$results = $this->db->get();
		return $results->result();
	}
	public function getMensajes(){
		$this->db->select("m.*");
		$this->db->from("mensaje m");
		$this->db->where("leido","no");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id", $id);
		$this->db->db_debug = false;
		if($this->db->delete("usuarios")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar productos que se han vendido!");
		}
	}

	public function getRols(){
		$this->db->select("*");
		$this->db->from("rol");
		$results = $this->db->get();
		return $results->result();
	}

	public function getId(){
		$this->db->select("u.id");
		$this->db->from("usuarios u");
		$this->db->order_by("u.id","desc");
		$this->db->limit(1);
		$result = $this->db->get();
		if($result->row()){
			return $result->row()->id+1;
		}else{
			return 0;
		}
	}

}