<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensajes_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE mensaje');
		return $this->db->insert("mensaje",$data);
	}

	public function update($data,$id){
		$this->db->where("id",$id);
		return $this->db->update("mensaje",$data);
	}

	public function getCorreo($id){
		$this->db->select("*");
		$this->db->from("mensaje");
		$this->db->where("id",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getCorreos(){
		$this->db->select("*");
		$this->db->from("mensaje");
		$this->db->order_by("leido","no");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id", $id);
		$this->db->db_debug = false;
		if($this->db->delete("mensaje")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar un cliente activo!");
		}
	}
	public function getUsuarios(){
		$this->db->select("*");
		$this->db->from("usuarios");
		$results = $this->db->get();
		return $results->result();
	}

}