<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

   
    
    public function getCants(){

        $usuario=$this->db->get("usuarios")->num_rows();
        $cliente=$this->db->get("clientes")->num_rows();
        $localidad=$this->db->get("localidades")->num_rows();
        $viatico=$this->db->get("viaticos")->num_rows();
        $vehiculo=$this->db->get("vehiculos")->num_rows();
        $vales=$this->db->get("vales")->num_rows();
        $distancias=$this->db->get("distancias")->num_rows();
        $ordenes=$this->db->get("ordenes")->num_rows();



		return (object) array(
            "cant_usuario"=>$usuario,
            "cant_cliente"=>$cliente,
            "cant_localidad"=>$localidad,
            "cant_viatico"=>$viatico,
            "cant_vehiculos"=>$vehiculo,
            "cant_vales"=>$vales,
            "cant_distancias"=>$distancias,
            "cant_ordenes"=>$ordenes,

        );
    }

    

}