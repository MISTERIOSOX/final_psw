<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE clientes');
		return $this->db->insert("clientes",$data);
	}

	public function update($data,$id){
		$this->db->where("id_cliente",$id);
		return $this->db->update("clientes",$data);
	}

	public function getClient($id){
		$this->db->select("c.*");
		$this->db->from("clientes c");
		$this->db->where("c.id_cliente",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getClients(){
		$this->db->select("c.*");
		$this->db->from("clientes c");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id_cliente", $id);
		$this->db->db_debug = false;
		if($this->db->delete("clientes")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar un cliente activo!");
		}
	}

}