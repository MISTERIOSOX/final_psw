<?php
if (!defined('BASEPATH'))
 exit('No direct script access allowed');

 class Impresion_viatico extends CI_Controller {
 public function __construct() {
 parent::__construct();
 $this->load->model('Viaticos_model');
 }
 
 
 /** FUNCIONES PARA IMPRIMIR *///
 public function imprimir($id=0) {
 ob_clean();
 $this->load->helper('pdf');
 $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
 $pdf->SetFont('helvetica', '', 10);
 $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . '',
PDF_HEADER_STRING);
 // set header and footer fonts
 $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
 $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
 // set default monospaced font
 $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
 //set margins
 $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
 $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
 $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
 //set auto page breaks
 $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 //set image scale factor
 $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
 $pdf->AddPage();
 // Header
 $viatico = $this->Viaticos_model->getViatico($id);
 $txt = $this->load->view("viaticos/detalle", array('viatico' => $viatico), true);
 $pdf->writeHTML($txt);
 //generar un codigo de barra
 $style = array(
 'position' => '',
 'align' => 'C',
 'stretch' => false,
 'fitwidth' => true,
 'cellfitalign' => '',
 'border' => true,
 'hpadding' => 'auto',
 'vpadding' => 'auto',
 'fgcolor' => array(0, 0, 0),
 'bgcolor' => false, //array(255,255,255),
 'text' => true,
 'font' => 'helvetica',
 'fontsize' => 8,
 'stretchtext' => 4
 );
 $valor=sha1($viatico->id_viatico);
 //$pdf->write1DBarcode('12345678901'+$valor, 'EAN13', '', '', '', 18, 0.4, $style, 'N');
 
 $pdf->write2DBarcode('Comprobante: '.$viatico->comprobante.'  Detalle: '.$viatico->detalle.'  importe:  '.$viatico->importe, "QRCODE,L", 80, 100, 50, 50, $style, "N");
 $pdf->Output('detalle_empleado_'.$viatico->id_viatico.'.pdf', 'I');
 $pdf->Text(120, 25, "QRCODE L");
 
 }
}
?>