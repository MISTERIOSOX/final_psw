<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model("Viaticos_model");
		$this->load->model("Log_model");
		$this->load->model("Usuario_model");
		if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{   
		$data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('viaticos/agregar');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/viaticos'); 
    }
    
    public function save(){

		$comprobante = $this->input->post("comprobante");
		$detalle = $this->input->post("detalle");
		$importe = $this->input->post("importe");
		$usuarioactual=$this->session->userdata("nombre");

		$this->form_validation->set_rules("comprobante","comprobante","required|min_length[3]");
        $this->form_validation->set_rules("detalle","detalle","required");
        $this->form_validation->set_rules("importe","importe","required");
		
		if ($this->form_validation->run()==TRUE) {
			$data  = array(
				'comprobante' => $comprobante,
                'detalle' => $detalle,
                'importe' => $importe,               
			);
			$data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Agregar Viaticos',
				'descripcion'=>'el usuario '.$usuarioactual.' agrego nuevo Viatico: '.$comprobante.'',

			);
			$this->Log_model->save($data2);

			$this->Viaticos_model->save($data);

			$this->session->set_flashdata("success","Se guardó correctamente!");
			redirect(base_url()."viaticos");
		}else{
			$this->load->view('layout/head');
			$this->load->view('layout/sidenav');
			$this->load->view('layout/topnav');
			$this->load->view('viaticos/agregar');
			$this->load->view('layout/footer');
			$this->load->view('layout/js/viaticos');
		}
		
	
	}

	
}
