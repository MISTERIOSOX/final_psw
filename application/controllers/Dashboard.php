<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Dashboard_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	  }

	  public function index()
	  {   
        
        $data2 = array("data2" => $this->Usuario_model->getMensajes(),
                       "cants"=>$this->Dashboard_model->getCants()
                      ); 
        
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('dashboard',$data2);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/dashboard');
    }

    
}
