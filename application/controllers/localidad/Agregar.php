<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model("Localidades_model");
		$this->load->model("Log_model");
		$this->load->model("Usuario_model");
		if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{   
		$data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('Localidades/agregar');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/localidades'); 
    }
    
    public function save(){

		$localidad = $this->input->post("localidad");
		$provincia = $this->input->post("provincia");
		$CP = $this->input->post("CP");
		$usuarioactual=$this->session->userdata("nombre");

		$this->form_validation->set_rules("localidad","localidad","required|min_length[3]|max_length[12]|alpha|trim");
        $this->form_validation->set_rules("provincia","provincia","required|min_length[3]|max_length[12]|alpha|trim");
		$this->form_validation->set_rules("CP","CP","required|min_length[2]|max_length[5]");
		
		if ($this->form_validation->run()==TRUE) {
			$data  = array(
				'localidad' => $localidad,
                'provincia' => $provincia,
                'CP' => $CP,               
			);
			$data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Agregar Localidad',
				'descripcion'=>'el usuario '.$usuarioactual.' agrego nueva Localidad: '.$localidad.'',

			);
			$this->Log_model->save($data2);

			$this->Localidades_model->save($data);

			$this->session->set_flashdata("success","Se guardó correctamente!");
			redirect(base_url()."localidades");
		}else{
			$this->load->view('layout/head');
			$this->load->view('layout/sidenav');
			$this->load->view('layout/topnav');
			$this->load->view('Localidades/agregar');
			$this->load->view('layout/footer');
			$this->load->view('layout/js/localidades');
		}
		
	
	}

	
}
