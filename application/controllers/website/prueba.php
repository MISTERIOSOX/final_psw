<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <!--Estilos css-->
    <link rel="stylesheet" href="css/estilos.css">
            <!--Bootstrap-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
            <!--Fuente-->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;1,300;1,400&display=swap" rel="stylesheet">
    
            <!--Jquery y Javascript-->
    <script src="js/jquery-3.5.1.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/parallax.js"></script>
    <!------------Iconos--------------->
    <script src="https://kit.fontawesome.com/2fd7ae33ae.js" crossorigin="anonymous"></script>

    <title>Forrajeria Guerrero</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg sticky-top" style="background-color: white;">
        <div class="container-fluid">
          <a class="navbar-brand" href="#" style="color: black;font-size: 25px;">Transportes Guerrero <img src="img/truck-2178252_1280.jpg" width="100px" alt=""> </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="fas fa-torah"style="font-size:30px"></i></span>
          </button>
          <div class=text-center collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mb-2 mb-lg-0 d-flex justify-content-end"style="width:100%">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#">Link</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#quien" tabindex="-1" aria-disabled="true">Quienes Somos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#his" tabindex="-1" aria-disabled="true">Nuestra Historia</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#ser" tabindex="-1" aria-disabled="true">Nuestros Servicios</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#cli" tabindex="-1" aria-disabled="true">Nuestros clientes</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#con" tabindex="-1" aria-disabled="true">Contactanos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" href="#inf" tabindex="-1" aria-disabled="true">Informacion Empresa</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <div class="container-fluid header">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="img/camiones-con-granos-w.png" alt="First slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/camion1.jpg" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="img/camion2.jpg" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    

   </div>

  
<div>
  <p id="quien"></p>
</div>

<div class="container-fluid" >
  <div class="row">
    <div class="col-md-6">
      <h3 class="h1 text-center">Cobertura <i class="fab fa-asymmetrik"></i></h3>
      <img src="img/cobertura.png"class="img-fluid my" alt="">
      <p class="p text-center my-3">.</p>
    </div>
    <div class="col-md-6">
      <div class="card my-5 mx-2">
        <div class="card-header bg-primary ">
         <h3 class="h3"><i class="bi bi-emoji-sunglasses"></i> ¿Quienes somos?</h3>
        </div>
        <div class="card-body">
          <h5 >Empresa Transporte Viruscool.</h5>
          <p class="card-text">Si bien nacimos en el Buenos Aires, hoy gracias a nuestros clientes, al hermoso equipo de trabajo y el esfuerzo de los proveedores amigos que nos apoyaron siempre, estamos en estamos en 3 provincias con mas de 10 tiendas físicas. También logramos desarrollar nuestro Centro de Distribución en Jujuy y en plena pandemia terminamos el nuevo Centro de Distribución en la ciudad de La Quiaca.</p>
          <p>En nuestros comienzos entendimos lo difícil de empezar de cero, sin contactos ni buenos costos, todo a prueba y error, donde nos hubiese encantado encontrar quién nos asesore. Desde ahí surge nuestro aporte donde todos pueden comprar lo necesario para sus proyectos, sus ideas y también para alimentarse de forma saludable y accesible.</p>
        </div>
      </div>
      <div class="card my-5 mx-2">
        <div class="card-header bg-primary ">
         <h3 class="h3"><i class="fas fa-times-circle" style="color: white;"></i> Politica de calidad</h3>
        </div>
        <div class="card-body">
          <h5 class="card-title">Empresa Transporte VIRUSCOOL.</h5>
          <p class="card-text">La dirección de GRUPO VIRUSCOOL asegura que se identifican las necesidades y expectativas de nuestros  clientes,que se convierten en requisitos y que se cumplen en su totalidad para alcanzar la satisfacción del cliente incluyendo las disposiciones legales y reglamentarias y superando así las expectativas creadas.

            Existe por parte de la dirección la decisión firme de integrar la calidad en la estrategia de la empresa y de disponer de técnicas y herramientas para perseguir una mejora continua.
            
            Tratamos de convencer a todos los que intervienen en la “espiral del progreso de la Calidad” y a todos cuantos componemos GRUPO VIRUSCOOL, de que un Sistema de Calidad significa una utilización más eficiente de los recursos empresariales: equipos, materiales, información, tiempo, etc., y por consiguiente supone costes más bajos y productividad más elevada.
            
            Entendemos que el Sistema de la Calidad involucra a todos los colaboradores de la empresa en cualquiera de sus funciones. Por este motivo, se presta gran atención a la sensibilización de todos los integrantes de la organización.
            
            Con los empleados Grupo Viruscool promoverá, vigilará y potenciará un buen ambiente de trabajo y trabajara para mantenerlos comprometidos con la organización y política, fomentando su formación y desarrollando su potencial con el fin de lograr sus metas profesionales. Velará por el cumplimiento de un plan que fomente la igualdad de trato y oportunidades en el ámbito laboral sin distinción de sexo o raza.
            
            Con los proveedores, colaboradores y agentes Grupo Ibertransit ofrecerá y exigirá lealtad, profesionalidad, confidencialidad y cooperación para desarrollar conjuntamente procedimientos y que mejoren nuestros costes de compra y de gestión confirmando que cumplen siempre con nuestros requisitos de calidad y seguridad como parte importante de nuestros procesos.
            
            Se concretarán objetivos que estén alineados con esta política y que además identificarán los requisitos, recursos y cronogramas para su consecución.
            
            Esta política será objeto de revisión periódica por la dirección para ajustarse a la realidad de cada momento y será comunicada a todo el personal que compone GRUPO VIRUSCOOL así como cualquier otra parte que pueda estar interesada en la misma.
            
            </p>
        </div>
      </div>
    </div>
  </div>
  <div>
    <p id="his"></p>
  </div>
  

  <div class="row" id="">
    <div class="col-12">
      <div class="card bg-dark text-white">
        <img src="img/camion-head.jpg" class="card-img" alt="nuestra historia">
        <div class="card-img-overlay d-flex flex-column align-items-center">
          <h1 class="card-title">Nuestra Historia</h1>
          <p class="card-text" style="text-align: center;">Forrajeria Guerrero es el resultado exitoso de una fusión de negocios orientados a ofrecer e implementar soluciones efectivas e innovadoras en operaciones logísticas y de transporte de cargas para el mercado, con una visión integradora sobre todos los segmentos que componen la cadena de abastecimiento, tanto en el ámbito local como internacional.

            Las companegocios que integraron esta fusión fueron desarrollando en forma progresiva a lo largo de muchos años de experiencia, alianzas estratégicas entre sí y ya finalmente consolidadas en conjunto se constituyen en una empresa referente de servicios.
            
            Integramos recursos humanos, tecnológicos, financieros, de infraestructura y equipamiento para transportar, almacenar y distribuir nuestros productos.
            
            En consecuencia diseñamos, planificamos y ejecutamos soluciones logísticas específicas orientadas a satisfacer los requerimientos de nuestros clientes.</p>
          
        </div>
      </div>
    </div>
  </div>
  
  <div class="row my-5" style="background-color:  rgb(168, 165, 165);" id="ser">
    <div class="col">
      <h1 class="h1 text-center">Servicios</h1>
      <div class="card-group my-5 text-center" style="background-color: gray;border: none;">
        <div class="card text-center" style="background-color:  rgb(168, 165, 165);border: none;">
          <i class="fas fa-archive" style="font-size:40px;color: blue;"></i>
          <div class="card-body">
            <h5 class="card-title">Alimentos para Mascotas</h5>
            <p class="card-text">Alimentos para perros, gatos, peces, tortugas, pájaros, hámsters. También pueden encontrarse para animales de tipo exótico.</p>
            
          </div>
        </div>
        <div class="card text-center" style="background-color: rgb(168, 165, 165);border: none;">
          <i class="fab fa-atlassian" style="font-size:40px;color: blue;"></i>
          <div class="card-body">
            <h5 class="card-title">Granos y Semillas </h5>
            <p class="card-text">Aqui podras encontrar rollos de alfalfa, maíz molido, maíz entero, avena, sorgo, avena.</p>
          </div>
        </div>
        <div class="card text-center" style="background-color:  rgb(168, 165, 165);border: none;">

          <i class="fas fa-bolt" style="font-size:40px;color: blue;"></i>
          <div class="card-body">
            <h5 class="card-title">Animales de Cria</h5>
            <p class="card-text">Disponemos de herraduras, clavos, vendas, riendas. cubos de alfalfa, alimentos para conejos, aves (pollos, gallinas), conejos, cerdos, terneros, bovinos.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12">
      <div class="card-group my-5 text-center" style="background-color:  rgb(168, 165, 165);border: none;">
        <div class="card text-center" style="background-color:  rgb(168, 165, 165);border: none;">
          <i class="fas fa-book-medical" style="font-size:40px;color: blue;"></i>
          <div class="card-body">
            <h5 class="card-title">Distribucion de Alimentos</h5>
            <p class="card-text">Contamos con 3 centros de distribucion asegurandonos proveer a nuestros clientes desde el punto estrategico mas cercano, brindando un servicio eficiente.</p>
            
          </div>
        </div>
        <div class="card text-center" style="background-color:  rgb(168, 165, 165);border: none;">
          <i class="fas fa-chart-pie" style="font-size:40px;color: blue;"></i>
          <div class="card-body">
            <h5 class="card-title">Accesorios para Mascotas</h5>
            <p class="card-text">Aca podras encontrar bandejas, correas, juguetes, cadenas, ropa, comederos, aletas, bebederos, jaulas, peceras y hasta línea cosmética.</p>
          </div>
        </div>
        <div class="card text-center" style="background-color:  rgb(168, 165, 165);border: none;">
          <i class="fas fa-chart-line" style="font-size:40px;color: blue;"></i>
          <div class="card-body">
            <h5 class="card-title">Ventas por Mayor</h5>
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque ipsa praesentium ut, nulla cupiditate nostrum.</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row" id="cli">
    <div class="col-12">
      <h1 class="h1 text-center">Nuestros clientes</h1>
      
        <h2 style="text-align: center;">Brindamos soluciones de logística y distribución de cargas, ofreciendo excelencia en calidad </h2>
        <h2 style="text-align: center;">y seguridad estableciendo relaciones de largo plazo con nuestros clientes.</h2>
      <br>
      
      <div class="card-group">
        <div class="card d-flex align-items-center card-marcas"style="border:none;">
          <img src="img/marca1.jpg" class="card-img-top "alt="...">
          
        </div>
        <div class="card d-flex align-items-center card-marcas "style="border:none;">
          <img src="img/marca2.jpg" class="card-img-top "alt="...">
          
        </div>
        <div class="card d-flex align-items-center card-marcas"style="border:none;">
          <img src="img/marca3.jpg" class="card-img-top "alt="...">
          
        </div>
      </div>
    </div>

    <div class="col-12">
      <div class="card-group">
        <div class="card d-flex align-items-center card-marcas" style="border:none;">
          <img src="img/marca4.png" class="card-img-top "alt="...">
          
        </div>
        <div class="card d-flex align-items-center card-marcas "style="border:none;">
          <img src="img/marca5.png" class="card-img-top "alt="...">
          
        </div>
        <div class="card d-flex align-items-center card-marcas text-center "style="border:none;">
          <img src="img/marca6.jpg" class="card-img-top "alt="...">
          
        </div>
      </div>
    </div>
  </div>

  <div class="row my-5" style="background-color:rgb(168, 165, 165);">
    <div class="col-md-6">
      <div class="card my-5 align-items-center" style="width:100%;border: none;background-color:rgb(168, 165, 165);">
        <ul class="list-group list-group-flush">
          <li class="list-group-item mb-3" style="border-bottom: 1px solid black;color:  rgb(70, 70, 240)"><h2 class="h2">Contactenos</h2></li>
          <li class="list-group-item"><i class="far fa-envelope"></i> servicios@gmail.com</li>
          <li class="list-group-item"><i class="fab fa-facebook"></i> Facebook</li>
          <li class="list-group-item"><i class="fas fa-phone"></i> (011)15-31710962</li>
        </ul>
      </div>
    </div>
    <div class="col-md-6 d-flex justify-content-center">
      <div class="card w-75 my-4 " style="border:none">
        <div class="card-body" >
          <h5 class="card-title h2" style="color: rgb(70, 70, 240);">Ingresa Tus datos</h5>
          <p class="card-text" style="font-weight: 500;">Resolveremos Tus dudas e inquietudes</p>
          <form>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Nombre</label>
              <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Mail</label>
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
              <label for="exampleFormControlTextarea1" class="form-label">Mensaje</label>
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  <footer>
    <section class="mapa">
      <h1 class="h1 my-5">Ubicacion</h1>
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14785.978562188364!2d-65.60637117995361!3d-22.107108666786186!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9406a41f6ab81251%3A0x7d22854bef4eda39!2sLa%20Quiaca%2C%20Jujuy!5e0!3m2!1ses-419!2sar!4v1612064487328!5m2!1ses-419!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </section>
  </footer>

  <div class="row" style="background-color:rgb(31, 29, 29) ;" id="inf">
    <div class="col-md-6">
      <div class="card my-5 align-items-center" style="width:100%;border: none;background-color:rgb(31, 29, 29);">
        <ul class="list-group list-group-flush"style="background-color:rgb(31, 29, 29);">
          <li class="list-group-item info-general" style="border-bottom: 1px solid white;color:white;"><h2 class="h3">Informacion de la empresa</h2></li>
          <li class="list-group-item info-general"><h2 class="h5">Representante legal</h2></li>
          <li class="list-group-item info-general"><p>Ing. Marcos Ferreira</p></li>
          <li class="list-group-item info-general">| <i class="fas fa-envelope"></i> ferreira.marcos.1988@gmail.com</li>
          <li class="list-group-item info-general">| <i class="fas fa-phone"></i> (011)31710962</li>
          <li class="list-group-item info-general">| <i class="fas fa-mobile-alt"></i>(3885)15-456-854)</li>
          
        </ul>
      </div>
    </div>
    <div class="col-md-6">
      <div class="card my-5 align-items-center" style="width:100%;border: none;background-color:rgb(31, 29, 29);">
        <ul class="list-group list-group-flush"style="background-color:rgb(31, 29, 29);">
          <li class="list-group-item mb-3 info-general" style="border-bottom: 1px solid white;color:white;"><h2 class="h2">Nos Ubicamos</h2></li>
          <li class="list-group-item info-general"><i class="fas fa-location-arrow"></i></li>
          <li class="list-group-item info-general">calle 1245 </li>
          <li class="list-group-item info-general">Argentina Jujuy CP4000</li>
          
        </ul>
    </div>
  </div>
  <p style="color: white;text-align: center;"><i class="fas fa-copyright"></i> 2020</p>
</div>
      
 







    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>