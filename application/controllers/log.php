<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class log extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Log_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
        }
        
    }

	public function index()
	{       
        $data = array("data" => $this->Log_model->getLogs()); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('logs/lista',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/log');
        
    }
    
    


}