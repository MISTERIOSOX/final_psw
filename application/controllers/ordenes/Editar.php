<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Ordenes_model");
        $this->load->model("Usuario_model");
        $this->load->model("Log_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
    }

	public function index($id)
	{       
        $data = $this->Ordenes_model->getOrden($id); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        if($data){

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('ordenes/editar',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/ordenes');
        }
    }
    public function update($id){

    
		$estado = $this->input->post("estado");
		
		$usuarioactual=$this->session->userdata("nombre");
		
					$data  = array(
			
				'estado' => $estado, 
				               
			);
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Editar Ordenes',
				'descripcion'=>'el usuario '.$usuarioactual.' Edito una orden para vehiculo: '.$vehiculo.'',

			);
			$this->Log_model->save($data2);

			$this->Ordenes_model->update($data,$id);
            $this->session->set_flashdata("success","Se modificó correctamente!");
            redirect(base_url()."ordenes");
            
		
	}
    

}
