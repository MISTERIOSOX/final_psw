<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Nuevo</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>chat">Correo</a></li>
                <li class="breadcrumb-item active">Nuevo</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>chat" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Nuevo Correo </h3>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form action="<?php echo base_url();?>nuevo-correo/save" method="POST">
                        
                        
                           
                                <div class="form-group">
                                    <label class="form-control-label">Usuario</label>
                                    <select id="usuario" name="usuario" class="form-control">
                                            </select>
                                    <div class="invalid-feedback"><?php echo form_error('usuario'); ?></div>
                                </div>
                                <div class="form-group">
                                            <label class="form-control-label">Asunto</label>
                                            <input type="text" name="asunto" class="form-control <?php echo form_error('asunto') ? 'is-invalid':''?>" placeholder="asunto"  value="<?php echo set_value('asunto'); ?>">
                                            <div class="invalid-feedback"><?php echo form_error('asunto'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Mensaje</label>
                                    <textarea rows="6" name="mensaje" class="form-control <?php echo form_error('mensaje') ? 'is-invalid':''?>" placeholder="Mensaje"><?php echo set_value('mensaje'); ?></textarea>
                                    <div class="invalid-feedback"><?php echo form_error('mensaje'); ?></div>
                                </div>
                                    
                                
                                 <div class="form-group text-right">
                                    <button type="submit" class="btn btn-success mt-4">Enviar Correo</button>
                                </div>
                          

                        
                    </form>
                </div>

            </div>
        </div>
    </div>
