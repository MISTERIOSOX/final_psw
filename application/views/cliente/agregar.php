<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Nuevo</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>clientes">Clientes</a></li>
                <li class="breadcrumb-item active">Nuevo</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>clientes" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Ingrese datos </h3>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form action="<?php echo base_url();?>nuevo-cliente/save" method="POST">
                        
                        
                           
                                <div class="form-group">
                                    <label class="form-control-label">Nombre</label>
                                    <input type="text" name="nombre" class="form-control <?php echo form_error('nombre') ? 'is-invalid':''?>" placeholder="Nombre del cliente"  value="<?php echo set_value('nombre'); ?>">
                                    <div class="invalid-feedback"><?php echo form_error('nombre'); ?></div>
                                </div>
                                <div class="form-group">
                                            <label class="form-control-label">Apellido</label>
                                            <input type="text" name="apellido" class="form-control <?php echo form_error('apellido') ? 'is-invalid':''?>" placeholder="apellido del cliente"  value="<?php echo set_value('apellido'); ?>">
                                            <div class="invalid-feedback"><?php echo form_error('apellido'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">Direccion</label>
                                    <input type="text" name="direccion" class="form-control <?php echo form_error('direccion') ? 'is-invalid':''?>" placeholder="Direccion del cliente"  value="<?php echo set_value('direccion'); ?>">
                                    <div class="invalid-feedback"><?php echo form_error('direccion'); ?></div>
                                </div>
                                    
                                
                                 <div class="form-group text-right">
                                    <button type="submit" class="btn btn-success mt-4">Guardar</button>
                                </div>
                          

                        
                    </form>
                </div>

            </div>
        </div>
    </div>
