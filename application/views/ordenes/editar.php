

<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0"></h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>ordenes">inicio</a></li>
                <li class="breadcrumb-item active"></li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>ordenes" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
    <div class="col-xl-8 order-xl-1">
        <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-8">
                    <h3 class="mb-0">Actualice los datos</h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="<?php echo base_url();?>ordenes/update/<?php echo $id_viaje; ?>" method="POST">
            <div class="form-group">
                                    <label class="form-control-label">Origen</label>
                                    <input readonly="readonly" type="text" name="origen" class="form-control <?php echo form_error('origen') ? 'is-invalid':''?>" placeholder="Origen"  value="<?php echo form_error('origen') ? set_value('origen'): $origen; ?>">
                                    <div class="invalid-feedback"><?php echo form_error('origen'); ?></div>
                                </div>

                                 <div class="form-group">
                                    <label class="form-control-label">Destino</label>
                                    <input readonly="readonly" type="text" name="destino" class="form-control <?php echo form_error('destino') ? 'is-invalid':''?>" placeholder="destino"  value="<?php echo form_error('destino') ? set_value('destino'): $destino; ?>">
                                    <div class="invalid-feedback"><?php echo form_error('destino'); ?></div>
                                </div>
                                                           
                                <div class="form-group">
                                    <label class="form-control-label">estado</label>
                                    <input readonly="readonly"type="text" name="" class="form-control <?php echo form_error('km') ? 'is-invalid':''?>" placeholder="kilometros"  value="<?php echo form_error('km') ? set_value('km'): $estado; ?>">
                                    <div class="invalid-feedback"><?php echo form_error('estado'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">cliente</label>
                                    <input readonly="readonly"type="text" name="km" class="form-control <?php echo form_error('cliente') ? 'is-invalid':''?>" placeholder="kilometros"  value="<?php echo form_error('cliente') ? set_value('cliente'): $cliente; ?>">
                                    <div class="invalid-feedback"><?php echo form_error('cliente'); ?></div>
                                </div>
                                <div class="form-group">
                                    <label class="form-control-label">vehiculo</label>
                                    <input readonly="readonly"type="text" name="vehiculo" class="form-control <?php echo form_error('vehiculo') ? 'is-invalid':''?>" placeholder="kilometros"  value="<?php echo form_error('vehiculo') ? set_value('vehiculo'): $vehiculo; ?>">
                                    <div class="invalid-feedback"><?php echo form_error('vehiculo'); ?></div>
                                </div>
                                <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Estado</label>
                                            <select id="estado" name="estado" class="form-control">
                                            <option value="en preparacion">En Preparacion</option>
                                            <option value="en viaje" selected>En viaje</option>
                                            <option value="en destino">En Destino</option>
                                            </select>
                                            <div class="invalid-feedback"><?php echo form_error('estado'); ?></div>
                                        </div>
                            </div>
                            <div class="form-group text-right">
                            <button type="submit" class="btn btn-success mt-4">Guardar</button>
                            </div>
            </form>
        </div>
        </div>
    </div>
    </div>
