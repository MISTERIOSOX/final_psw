      <!-- Footer -->
      <br>
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-1">
            <div class="copyright text-center  text-lg-left  text-muted">
             
            </div>
            </div>
          <div class="col-lg-5">
            <div class="copyright text-center  text-lg-left  text-muted">
              SISTEMA DE COMBUSTIBLE DESARROLLADO POR<br/>
              &copy; 2021 <a href="" class="font-weight-bold ml-1" target="_blank">Ferreira Marcos, Duran Miguel</a>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              GRUPO 1 PROGRAMACION Y SERVICIOS WEB <br/>
              &copy; 2021 <a href="" class="font-weight-bold ml-1" target="_blank">VIRUSCOOL</a>
            </div>
          </div>

        </div>
      </footer>
    </div>
  </div>
 
  <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/jquery-scroll-lock/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="<?php echo base_url(); ?>assets/vendor/chart.js/Chart.min.js"></script>
  <!-- Datatable -->
  <script src="<?php echo base_url(); ?>assets/vendor/datatables.net/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/vendor/datatables.net-responsive-bs4/js/dataTables.bootstrap4.min.js"></script>
  <!-- Sweet Alert 2 -->
  <script src="<?php echo base_url(); ?>assets/vendor/sweetalert2/sweetalert2.min.js"></script>
  <!-- Argon JS -->
  <script src="<?php echo base_url(); ?>assets/js/argon.js?v=1.2.0"></script>
  <!--<script src="<?= base_url(); ?>assets/js/dista.js"></script>-->
  
 
</body>

</html>