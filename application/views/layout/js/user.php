<script>
    $(document).ready( function () {
        $('#usuarioTable').DataTable();
        $(".nav-usuario").addClass("active");
                

        rol();

    
        
        <?php if($this->session->flashdata("success")):?>
        Swal.fire({
            position: 'top-end',
            type: 'success',
            title: '<?php echo $this->session->flashdata("success"); ?>',
            showConfirmButton: false,
            timer: 2000
        })
        <?php endif; ?>

        <?php if($this->session->flashdata("error")):?>
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: '<?php echo $this->session->flashdata("error") ?>',
            })
        <?php endif; ?>

    });
    


    

    function rol() {
        $.ajax({
            url: "<?php echo base_url(); ?>usuario/Lista/getData",
            type:"POST",
            dataType:"json",
            success:function(resp){


                var html=new Array();

                $.each(resp,function(key, value){
                    
                    if(value.id_rol ==<?php echo set_value('rolId') ? set_value('rolId'): (!empty($rol_id) ? $rol_id: 0) ?>){
                        html.push('<option  value="'+value.descripcion_rol+'" selected>'+value.descripcion_rol+'</option>');
                    }else{
                        html.push('<option  value="'+value.descripcion_rol+'">'+value.descripcion_rol+'</option>');
                    }

                });

                $("#rol").html(html);

            }
        });
    }
</script>