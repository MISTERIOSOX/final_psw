<script>
    $(document).ready( function () {

        $(".navbar-top").addClass("bg-default");



        <?php if(file_exists("./assets/img/user/".$this->session->userdata("picture"))): ?>
            $(".header").css({
              "background-image":
              "url(<?php echo base_url(); ?>assets/img/user/<?php echo $this->session->userdata("picture"); ?>)"
            });
        <?php else: ?>
            $(".header").css({
              "background-image":
              "url(<?php echo base_url(); ?>assets/img/user/img.png)"
            });
        <?php endif ?>

        <?php if($this->session->flashdata("success")):?>
        Swal.fire({
            position: 'top-end',
            type: 'success',
            title: '<?php echo $this->session->flashdata("success"); ?>',
            showConfirmButton: false,
            timer: 2000
        })
        <?php endif; ?>

       
    });
    
    
</script>