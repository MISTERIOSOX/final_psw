-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-02-2021 a las 01:50:00
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `combustible`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
`id_cliente` int(11) NOT NULL,
  `apellido` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `modificacion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distancias`
--

CREATE TABLE IF NOT EXISTS `distancias` (
`id_distancia` int(11) NOT NULL,
  `origen` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `destino` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `km` int(11) NOT NULL,
  `modificacion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `distancias`
--

INSERT INTO `distancias` (`id_distancia`, `origen`, `destino`, `km`, `modificacion`) VALUES
(2, 'jujuy', 'la quiaca', 300, NULL),
(3, 'jujuy', 'humahuaca', 160, NULL),
(4, 'humahuaca', 'la quiaca', 160, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidades`
--

CREATE TABLE IF NOT EXISTS `localidades` (
`id_localidad` int(11) NOT NULL,
  `localidad` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `provincia` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `CP` int(11) NOT NULL,
  `modificacion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `localidades`
--

INSERT INTO `localidades` (`id_localidad`, `localidad`, `provincia`, `CP`, `modificacion`) VALUES
(1, 'La Quiaca', 'JUJUY', 4650, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
`idlog` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `evento` varchar(256) NOT NULL,
  `descripcion` varchar(256) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `logs`
--

INSERT INTO `logs` (`idlog`, `fecha`, `hora`, `evento`, `descripcion`) VALUES
(1, '2021-01-31', '04:30:37', 'inicio de sesion', '0'),
(2, '2021-01-31', '04:32:27', 'inicio de sesion', 'el usuario admin ingreso'),
(3, '2021-01-31', '04:41:12', 'inicio de sesion', 'el usuario 4 agrego nuevo usuario'),
(4, '2021-01-31', '04:43:33', 'inicio de sesion', 'el usuario marcos agrego nuevo usuario'),
(5, '2021-01-31', '04:44:49', 'inicio de sesion', 'el usuario marcos agrego nuevo usuario: nieva123'),
(6, '2021-02-01', '04:37:35', 'inicio de sesion', 'el usuario admin ingreso'),
(7, '2021-02-01', '04:38:09', 'Agregar Cliente', 'el usuario marcos Agrego al cliente: pachecomaria'),
(8, '2021-02-01', '04:40:55', 'Editar Cliente', 'el usuario  Edito al cliente: pachecomaria beatriz'),
(9, '2021-02-01', '04:52:30', 'inicio de sesion', 'el usuario viruscool ingreso'),
(10, '2021-02-01', '04:56:12', 'inicio de sesion', 'el usuario admin ingreso'),
(11, '2021-02-02', '04:26:07', 'inicio de sesion', 'el usuario admin ingreso'),
(12, '2021-02-02', '05:36:07', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: 1'),
(13, '2021-02-02', '07:00:24', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: 1'),
(14, '2021-02-02', '07:00:37', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: 1'),
(15, '2021-02-02', '07:01:30', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: 1'),
(16, '2021-02-02', '07:21:06', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: 1'),
(17, '2021-02-02', '07:21:16', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: 1'),
(18, '2021-02-02', '08:00:08', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(19, '2021-02-02', '08:00:53', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(20, '2021-02-02', '08:02:18', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(21, '2021-02-02', '08:02:41', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(22, '2021-02-02', '08:03:25', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(23, '2021-02-02', '09:08:09', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(24, '2021-02-02', '09:11:30', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(25, '2021-02-02', '07:40:37', 'inicio de sesion', 'el usuario admin ingreso'),
(26, '2021-02-02', '07:41:00', 'Agregar Vales', 'el usuario marcos agrego nuevo vale: 4564'),
(27, '2021-02-02', '07:41:51', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(28, '2021-02-02', '07:42:08', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(29, '2021-02-03', '02:26:21', 'inicio de sesion', 'el usuario admin ingreso'),
(30, '2021-02-03', '02:27:40', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(31, '2021-02-03', '02:28:50', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(32, '2021-02-03', '02:32:56', 'Agregar Distancia', 'el usuario marcos agrego nueva Distancia desde: humahuaca hasta: la quiaca'),
(33, '2021-02-03', '02:34:09', 'Editar Distancia', 'el usuario marcos\r\n                \r\n                \r\n                \r\n                Distancia desde: humahuaca hasta: pumahuasi'),
(34, '2021-02-03', '02:40:20', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(35, '2021-02-03', '02:48:43', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(36, '2021-02-03', '02:48:59', 'Enviar mensaje', 'el usuario marcos envio un nuevo mensaje a: admin'),
(37, '2021-02-03', '02:52:28', 'Agregar Vales', 'el usuario marcos agrego nuevo vale: 4324324'),
(38, '2021-02-03', '02:56:11', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: viruscool11'),
(39, '2021-02-03', '04:56:19', 'inicio de sesion', 'el usuario admin ingreso'),
(40, '2021-02-03', '04:57:32', 'inicio de sesion', 'el usuario admin ingreso'),
(41, '2021-02-03', '04:48:44', 'inicio de sesion', 'el usuario admin ingreso'),
(42, '2021-02-04', '02:03:06', 'inicio de sesion', 'el usuario admin ingreso'),
(43, '2021-02-04', '02:08:09', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: marcusbeth01'),
(44, '2021-02-04', '02:36:13', 'Agregar Distancia', 'el usuario marcos agrego nueva Distancia desde: fdsf hasta: sfdfs'),
(45, '2021-02-04', '02:40:23', 'Agregar Vehiculo', 'el usuario marcos agrego nuevo Vehiculo: 1'),
(46, '2021-02-04', '02:41:58', 'Agregar Vehiculo', 'el usuario marcos agrego nuevo Vehiculo: aaaaaaa'),
(47, '2021-02-05', '12:22:25', 'inicio de sesion', 'el usuario admin ingreso'),
(48, '2021-02-05', '12:52:36', 'inicio de sesion', 'el usuario admin ingreso'),
(49, '2021-02-05', '12:53:45', 'inicio de sesion', 'el usuario admin ingreso'),
(50, '2021-02-05', '01:00:15', 'inicio de sesion', 'el usuario admin ingreso'),
(51, '2021-02-05', '01:01:11', 'inicio de sesion', 'el usuario admin ingreso'),
(52, '2021-02-05', '01:08:25', 'inicio de sesion', 'el usuario admin ingreso'),
(53, '2021-02-05', '01:36:06', 'Agregar Ordenes', 'el usuario marcos Agrego una orden para vehiculo: 0'),
(54, '2021-02-05', '02:00:23', 'inicio de sesion', 'el usuario admin ingreso'),
(55, '2021-02-05', '02:10:44', 'Agregar Vales', 'el usuario marcos agrego nuevo vale: 154556'),
(56, '2021-02-05', '03:26:05', 'Agregar Viaticos', 'el usuario marcos agrego nuevo Viatico: 154523'),
(57, '2021-02-05', '04:07:46', 'inicio de sesion', 'el usuario admin ingreso'),
(58, '2021-02-05', '04:12:24', 'inicio de sesion', 'el usuario admin ingreso'),
(59, '2021-02-05', '04:12:56', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: dana1'),
(60, '2021-02-05', '04:17:15', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: cussi12'),
(61, '2021-02-05', '04:19:02', 'inicio de sesion', 'el usuario admin ingreso'),
(62, '2021-02-05', '04:28:33', 'inicio de sesion', 'el usuario admin ingreso'),
(63, '2021-02-05', '04:29:50', 'inicio de sesion', 'el usuario admin ingreso'),
(64, '2021-02-05', '04:38:49', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: duran1'),
(65, '2021-02-05', '04:42:59', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: admin1233'),
(66, '2021-02-05', '04:43:31', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: admin1233'),
(67, '2021-02-05', '04:44:43', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: hkjhjkjhkjh'),
(68, '2021-02-05', '04:46:37', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: dwasfasfsd'),
(69, '2021-02-05', '04:50:58', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: admin2'),
(70, '2021-02-05', '04:52:07', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: viruscool'),
(71, '2021-02-05', '10:47:01', 'inicio de sesion', 'el usuario admin ingreso'),
(72, '2021-02-06', '12:40:09', 'Agregar usuario', 'el usuario marcos agrego nuevo usuario: duran1'),
(73, '2021-02-06', '12:41:22', 'Editar usuario', 'el usuario marcos edito al usuario: admin'),
(74, '2021-02-06', '12:42:30', 'Agregar Cliente', 'el usuario marcos Agrego al cliente: fernandezmarcos'),
(75, '2021-02-06', '12:42:45', 'Editar Cliente', 'el usuario  Edito al cliente: hernandezmarcos'),
(76, '2021-02-06', '12:43:33', 'Agregar Cliente', 'el usuario marcos Agrego al cliente: FerreiradsdMarsdd'),
(77, '2021-02-06', '12:43:59', 'Agregar Cliente', 'el usuario marcos Agrego al cliente: pachecoMarcos'),
(78, '2021-02-06', '12:44:52', 'Agregar Cliente', 'el usuario marcos Agrego al cliente: miguelduran'),
(79, '2021-02-06', '12:45:26', 'Agregar Cliente', 'el usuario marcos Agrego al cliente: sfdgadhnsdMarcos'),
(80, '2021-02-06', '12:59:19', 'Agregar Cliente', 'el usuario marcos Agrego al cliente: duranMauro'),
(81, '2021-02-06', '01:03:56', 'inicio de sesion', 'el usuario admin ingreso'),
(82, '2021-02-06', '01:08:42', 'Editar usuario', 'el usuario Marcos edito al usuario: admin'),
(83, '2021-02-06', '01:08:53', 'inicio de sesion', 'el usuario admin ingreso'),
(84, '2021-02-06', '01:09:08', 'Agregar Distancia', 'el usuario Marcos agrego nueva Distancia desde: jujuy hasta: humahuaca'),
(85, '2021-02-06', '01:10:57', 'Editar Distancia', 'el usuario Marcosedito Distancia desde: jujuy hasta: humahuaca'),
(86, '2021-02-06', '01:11:16', 'Agregar Vehiculo', 'el usuario Marcos agrego nuevo Vehiculo: aaaaaaa'),
(87, '2021-02-06', '01:12:26', 'Agregar Localidad', 'el usuario Marcos agrego nueva Localidad: Quiaca'),
(88, '2021-02-06', '01:14:36', 'Agregar Vales', 'el usuario Marcos agrego nuevo vale: 4324324'),
(89, '2021-02-06', '01:14:54', 'Agregar Viaticos', 'el usuario Marcos agrego nuevo Viatico: 4324324'),
(90, '2021-02-06', '01:41:09', 'inicio de sesion', 'el usuario admin ingreso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE IF NOT EXISTS `mensaje` (
`id` int(24) NOT NULL,
  `de` varchar(24) NOT NULL,
  `para` varchar(24) NOT NULL,
  `asunto` varchar(256) NOT NULL,
  `mensaje` varchar(256) NOT NULL,
  `fecha_envio` date NOT NULL,
  `hora_envio` time NOT NULL,
  `leido` varchar(2) NOT NULL,
  `fecha_lectura` date DEFAULT NULL,
  `hora_lectura` time DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mensaje`
--

INSERT INTO `mensaje` (`id`, `de`, `para`, `asunto`, `mensaje`, `fecha_envio`, `hora_envio`, `leido`, `fecha_lectura`, `hora_lectura`) VALUES
(9, 'admin', 'admin', 'hola marcos como estas', 'sa', '2021-02-02', '00:00:08', 'si', '2021-02-02', '08:09:37'),
(10, 'admin', 'admin', 'hola marcos como estas', 'as', '2021-02-02', '09:08:09', 'si', '2021-02-02', '09:25:12'),
(11, 'admin', 'admin', 'hola marcos como estas', 'asa', '2021-02-02', '09:11:30', 'si', '2021-02-02', '07:41:24'),
(12, 'admin', 'admin', 'hola marcos como estas', 'ds', '2021-02-02', '19:41:51', 'si', '2021-02-02', '07:42:49'),
(17, 'admin', 'admin', 'hola marcos como estas', 'ee', '2021-02-03', '02:48:43', 'si', '2021-02-03', '04:54:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenes`
--

CREATE TABLE IF NOT EXISTS `ordenes` (
`id_viaje` int(11) NOT NULL,
  `origen` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `destino` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `cliente` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `vehiculo` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `conductor` varchar(256) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ordenes`
--

INSERT INTO `ordenes` (`id_viaje`, `origen`, `destino`, `estado`, `cliente`, `vehiculo`, `conductor`) VALUES
(6, 'jujuy', 'la quiaca', 'value2', 'Ferreira', '0', 'ferreira');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
`id_rol` int(11) NOT NULL,
  `descripcion_rol` varchar(25) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `descripcion_rol`) VALUES
(1, 'administrador'),
(2, 'empleado'),
(3, 'conductor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id` int(11) NOT NULL,
  `apellido` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `rol` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `direccion` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `modificacion` int(11) DEFAULT NULL,
  `hash` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `apellido`, `nombre`, `rol`, `usuario`, `password`, `telefono`, `direccion`, `modificacion`, `hash`) VALUES
(1, 'Ferreira', 'Marcos', 'administrador', 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 1131710964, 'Av Paraguay 325', 2021, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vales`
--

CREATE TABLE IF NOT EXISTS `vales` (
`id_vales` int(11) NOT NULL,
  `comprobante` int(11) NOT NULL,
  `litros` int(11) NOT NULL,
  `importe` int(11) NOT NULL,
  `modificacion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `vales`
--

INSERT INTO `vales` (`id_vales`, `comprobante`, `litros`, `importe`, `modificacion`) VALUES
(1, 154556, 35, 1000, NULL),
(2, 4324324, 35, 5000, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE IF NOT EXISTS `vehiculos` (
`id_vehiculo` int(11) NOT NULL,
  `marca` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `modelo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `patente` varchar(11) COLLATE utf8_spanish_ci NOT NULL,
  `tanque` int(11) NOT NULL,
  `modificacion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id_vehiculo`, `marca`, `modelo`, `patente`, `tanque`, `modificacion`) VALUES
(1, 'ford', 'ka', '0', 4566465, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viaticos`
--

CREATE TABLE IF NOT EXISTS `viaticos` (
`id_viatico` int(11) NOT NULL,
  `comprobante` int(11) NOT NULL,
  `detalle` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `importe` int(11) NOT NULL,
  `modificacion` varchar(256) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `viaticos`
--

INSERT INTO `viaticos` (`id_viatico`, `comprobante`, `detalle`, `importe`, `modificacion`) VALUES
(1, 154523, 'ahgsahjgs', 1000, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
 ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `distancias`
--
ALTER TABLE `distancias`
 ADD PRIMARY KEY (`id_distancia`);

--
-- Indices de la tabla `localidades`
--
ALTER TABLE `localidades`
 ADD PRIMARY KEY (`id_localidad`);

--
-- Indices de la tabla `logs`
--
ALTER TABLE `logs`
 ADD PRIMARY KEY (`idlog`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ordenes`
--
ALTER TABLE `ordenes`
 ADD PRIMARY KEY (`id_viaje`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
 ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vales`
--
ALTER TABLE `vales`
 ADD PRIMARY KEY (`id_vales`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
 ADD PRIMARY KEY (`id_vehiculo`), ADD UNIQUE KEY `patente` (`patente`);

--
-- Indices de la tabla `viaticos`
--
ALTER TABLE `viaticos`
 ADD PRIMARY KEY (`id_viatico`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `distancias`
--
ALTER TABLE `distancias`
MODIFY `id_distancia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `localidades`
--
ALTER TABLE `localidades`
MODIFY `id_localidad` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `logs`
--
ALTER TABLE `logs`
MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
MODIFY `id` int(24) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `ordenes`
--
ALTER TABLE `ordenes`
MODIFY `id_viaje` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `vales`
--
ALTER TABLE `vales`
MODIFY `id_vales` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
MODIFY `id_vehiculo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `viaticos`
--
ALTER TABLE `viaticos`
MODIFY `id_viatico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
